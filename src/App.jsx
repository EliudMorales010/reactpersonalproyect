import './App.css'
import ComponenteBody from './Componentes/ComponenteBody'
import ComponenteFooter from './Componentes/ComponenteFooter'
import ComponenteHeader from './Componentes/ComponenteHeader'


function App() {
  return (
    <div>
      <ComponenteHeader/>
      <ComponenteBody/>
      <ComponenteFooter/>
    </div>
  )
}

export default App
