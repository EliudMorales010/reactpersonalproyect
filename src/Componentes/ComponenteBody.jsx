import React from "react";
import "./StylesBody.css";

function ComponenteBody() {
  return (
    <div>
      <section className="py-5">
        <div className="container px-4 px-lg-5 my-5">
          <div className="row gx-4 gx-lg-5 align-items-center">
            {/* imagen lado izquierdo */}
            <div className="col-md-6">
              <img className="card-img-top mb-5 mb-md-0" src="./public/eliud.jpeg" alt="..." />
            </div>
            <div className="col-md-6">
              {/* lado derecho */}
              <h1 className="display-5 fw-bolder">Hola soy Eliud</h1>
              <p className="lead" >
                Tengo 21 años, soy estudiante de Ing en Sistemas
                Computacionales, trabajo en Pan y Vino como Bartender, ubicado
                en Cuautla Morelos, en mi carrera actual, tengo un gusto por la
                ciencia de datos y la inteligencia Artificial.<br></br>
                <br />
              <p className="leaddos">
                Me gusta mucho pasar tiempo, con mi familia y mi novia, tengo muchas metas a futuro, 
                dentro de ellas tener mi propio negocio, y mi propia casa, un dato curioso es que me gustaria estudiar muchas 
                carreras tecnicas, me considero muy bueno reparando cosas, armando y desarmando cualquier cosa, 
                le tengo un amor por los coches muy grande que aprendi a manejar a los 12 años, recuerdo aun ese sentimiento tan bonito. 
              </p>
              </p>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default ComponenteBody;
