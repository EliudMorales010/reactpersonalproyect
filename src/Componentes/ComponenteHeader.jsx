import React from 'react'
import './StylesHeader.css'

function ComponenteHeader() {
  return (    
  <div className='pos-f-t py-4 bg-dark' >
    <ul class="nav nav-pills nav-fill">
      <li class="nav-item">
        <a class="nav-link" href="#">Biografia</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Fotos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pasatiempos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Metas</a>
      </li>
    </ul>
  </div>
  )
}

export default ComponenteHeader