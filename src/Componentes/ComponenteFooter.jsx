import React from "react";
import "./StylesFooter.css";

function ComponenteFooter() {
  return (
    <div>
      <footer className="py-4 bg-dark">
        <center>
          <h1 className="titulo">Redes sociales</h1>
          {/* Facebook */}
          <a
            className="btn btn-outline-light btn-floating m-1" href="https://es-la.facebook.com" role="button">
            <i className="fa fa-facebook"></i>
          </a>
          {/* Twitter */}
          <a className="btn btn-outline-light btn-floating m-1" href="https://twitter.com/?lang=es" role="button"
            ><i className="fa fa-twitter"></i>
          </a>
          {/* Google */}
          <a className="btn btn-outline-light btn-floating m-1" href="https://www.google.com/intl/es-419/gmail/about/" role="button"
            ><i className="fa fa-google"></i>
          </a>
          {/* Instagram */}
          <a className="btn btn-outline-light btn-floating m-1" href="https://www.instagram.com" role="button"
            ><i className="fa fa-instagram"></i>
          </a>
          {/* Linkedin */}
          <a className="btn btn-outline-light btn-floating m-1" href="https://mx.linkedin.com" role="button"
            ><i className="fa fa-linkedin"></i>
          </a>
          {/* GitHub */}
          <a className="btn btn-outline-light btn-floating m-1" href="https://github.com" role="button"
            ><i className="fa fa-github"></i>
          </a>
        </center>
      </footer>
    </div>
  );
}

export default ComponenteFooter;
